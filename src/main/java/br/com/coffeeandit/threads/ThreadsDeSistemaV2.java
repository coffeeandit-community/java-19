package br.com.coffeeandit.threads;

import java.lang.management.ManagementFactory;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class ThreadsDeSistemaV2 {
    public static void main(String[] args) {

        // Para monitorar o número de Threads do Sistema usados pelo teste, escreva o seguinte código:
        var scheduledExecutorService = Executors.newScheduledThreadPool(1);
        ScheduledFuture<?> scheduledFuture = null;
        var atomicInteger = new AtomicInteger();
        ScheduledFuture<?> finalScheduledFuture = scheduledFuture;
        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(() -> {

            var threadBean = ManagementFactory.getThreadMXBean();
            var threadInfo = threadBean.dumpAllThreads(false, false);
            System.out.println(threadInfo.length + " Threads de Sistema \n");
            if (Objects.nonNull(finalScheduledFuture) && atomicInteger.get() > 100) {
                finalScheduledFuture.cancel(true);
            }
            atomicInteger.getAndIncrement();
        }, 10, 10, TimeUnit.MILLISECONDS);
        scheduledExecutorService.shutdown();
        long l = System.currentTimeMillis();

        // Agora usamos um pool de threads com tamanho fixo de 200 para resolver o problema de não aplicar muitos threads do sistema:
        try (var executor = Executors.newFixedThreadPool(200) ) {
            IntStream.range(0, 1000).forEach(i -> {
                executor.submit(() -> {
                    Thread.sleep(Duration.ofSeconds(1));
                    System.out.println(i);
                    return i;
                });
            });
            executor.shutdown();
        }

        System.out.printf("Tempo de execução: %dms\n", System.currentTimeMillis() - l);
    }
}