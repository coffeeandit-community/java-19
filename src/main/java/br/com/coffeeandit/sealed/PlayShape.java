package br.com.coffeeandit.sealed;

public class PlayShape {

    public static void main(String[] args) {
        System.out.println("circle = "+ surface(new Circle(1d)));
    }

    public static double surface(Shape shape) {
        return switch(shape) {
            case Circle circle -> Math.PI * circle.getRadius() * circle.getRadius();
            case Square square -> square.getEdge() * square.getEdge();
            case Rectangle rectangle -> rectangle.height() * rectangle.width();
            default -> 0d;
        };
    }

}
