package br.com.coffeeandit.sealed;

public record Rectangle(double width, double height) implements Shape {
}
