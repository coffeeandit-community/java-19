package br.com.coffeeandit.sealed;

public sealed interface Shape permits Circle, Rectangle, Square {

}
