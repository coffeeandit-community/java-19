package br.com.coffeeandit.sealed;

public final class Square implements Shape {

    private double edge;

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }
}
