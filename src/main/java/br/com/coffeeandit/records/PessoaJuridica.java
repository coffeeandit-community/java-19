package br.com.coffeeandit.records;

public record PessoaJuridica(String nome, long cnpj, Endereco endereco) {
}
