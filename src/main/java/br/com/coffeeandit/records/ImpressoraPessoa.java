package br.com.coffeeandit.records;

public class ImpressoraPessoa {

    public void printObject(final Object pessoa) {
        if (pessoa instanceof PessoaFisica pessoaFisica) {
            print(pessoaFisica.nome(), pessoaFisica.cpf(), pessoaFisica.endereco());
        } else if (pessoa instanceof PessoaJuridica pessoaJuridica) {
            print(pessoaJuridica.nome(), pessoaJuridica.cnpj(), pessoaJuridica.endereco());
        }
    }

    public void printRecords(final Object pessoa) {
        if (pessoa instanceof PessoaFisica(String nome,long cpf,Endereco endereco)) {
            print(nome, cpf, endereco);
        } else if (pessoa instanceof PessoaJuridica(String nome,long cnpj,Endereco endereco)) {
            print(nome, cnpj, endereco);
        }
    }

    public void printSwitch(final Object pessoa) {
        // Pattern Matching
        switch (pessoa) {
            case PessoaFisica(String nome,long cpf,Endereco endereco) -> print(nome, cpf, endereco);
            case PessoaJuridica(String nome,long cnpj,Endereco endereco) -> print(nome, cnpj, endereco);
            default -> throw new IllegalStateException("Unexpected value: " + pessoa);
        }
    }

    private void print(final String nome, long documento, final Endereco endereco) {
        System.out.println("o documento da pessoa " + nome + " é " + documento + " domicialiada no endereço: " + endereco.rua() + " de número: " + endereco.numero());
        System.out.println();
    }
}
