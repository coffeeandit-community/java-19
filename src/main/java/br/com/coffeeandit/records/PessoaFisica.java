package br.com.coffeeandit.records;

public record PessoaFisica(String nome, long cpf, Endereco endereco) {
}
