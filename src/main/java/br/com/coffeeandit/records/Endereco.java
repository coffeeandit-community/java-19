package br.com.coffeeandit.records;

public record Endereco(String rua, long numero) {
}
