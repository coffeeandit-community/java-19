package br.com.coffeeandit.records;

public class RecordsMain {

    public static void main(String[] args) {


        var endereco = new Endereco("Rua 1", 1);

        var pessoaFisica =
                new PessoaFisica("Cléber da Silveira", 26, endereco);
        var pessoaJuridica =
                new PessoaJuridica("CoffeeAndIT", 26, endereco);

        var impressoaPessoa = new ImpressoraPessoa();

        impressoaPessoa.printObject(pessoaFisica);
        impressoaPessoa.printObject(pessoaJuridica);


    }
}
